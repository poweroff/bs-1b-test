#!/usr/bin/env python3

from argparse import ArgumentParser

from battleshiptest import BattleshipTest


def main():
    parser = ArgumentParser(
        description="ShipMaster is a simple tool used to test battleship "
        " clients."
    )
    parser.add_argument("client", type=str, help="Client binary")
    parser.add_argument("server", type=str, help="Server binary")
    parser.add_argument("-n", "--samples", dest="samples", type=int,
                        default=200, help="Number of tests.")
    parser.add_argument("-v", "--verbose", action="store_true",
                        help="Print debug information.")
    args = parser.parse_args()

    bt = BattleshipTest(args.client,
                        args.server,
                        samples=args.samples,
                        verbose=args.verbose)
    bt.start_test()


if __name__ == "__main__":
    main()
