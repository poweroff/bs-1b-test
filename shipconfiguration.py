import copy


class Field(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def distance_to(self, other):
        if self.x == other.x:
            d = self.y - other.y
        else:
            d = self.x - other.x

        return abs(d) + 1

    def validate(self, size):
        validx = (0 <= self.x < size)
        validy = (0 <= self.y < size)

        return validx and validy

    def __str__(self):
        x = chr(ord("A") + self.x)
        y = chr(ord("0") + self.y)

        return "{x}{y}".format(x=x, y=y)

    def __repr__(self):
        return "<Field x: {}, y: {}>".format(self.x, self.y)


class Ship(object):
    def __init__(self, startx, starty, endx, endy):
        if startx != endx and starty != endy:
            raise ShipCreationError()

        if startx > endx:
            startx, endx = endx, startx
        if starty > endy:
            starty, endy = endy, starty

        self.start = Field(startx, starty)
        self.end = Field(endx, endy)
        self.length = self.start.distance_to(self.end)

        if not 2 <= self.length <= 4:
            raise ShipCreationError()

    def validate(self, size):
        return self.start.validate(size) and self.end.validate(size)

    def __str__(self):
        return "{s}{e}".format(s=self.start, e=self.end)

    def __repr__(self):
        return "<Ship start: {}, end: {}, length: {}>".format(
            self.start,
            self.end,
            self.length
        )


class ShipConfiguration(object):
    def __init__(self, size=10):
        self.size = size
        self.map = [[" " for x in range(size)] for y in range(size)]
        self.ships = []

    def _fit_ship(self, ship):
        if not ship.validate(self.size):
            return False

        m = copy.deepcopy(self.map)

        for x in range(ship.start.x - 1, ship.end.x + 2):
            for y in range(ship.start.y - 1, ship.end.y + 2):
                f = Field(x, y)
                if f.validate(self.size):
                    if m[x][y] != " ":
                        return False

        return True

    def add_ship(self, ship):
        if self._fit_ship(ship):
            self.ships.append(ship)

            for x in range(ship.start.x, ship.end.x + 1):
                for y in range(ship.start.y, ship.end.y + 1):
                    self.map[x][y] = "x"

            return True
        else:
            return False

    def __str__(self):
        return " ".join(map(str, self.ships))

    def __repr__(self):
        m = "  "
        for x in range(ord("A"), ord("A") + self.size):
            m += "{} ".format(chr(x))
        m += "\n"

        for y in range(self.size):
            m += "{} ".format(y)
            for x in range(self.size):
                m += "{} ".format(self.map[x][y])
            m += "\n"

        return m


class ShipCreationError(Exception):
    pass
