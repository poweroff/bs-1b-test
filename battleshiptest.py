import sys
import random
import re
import logging
import json

import subprocess
import shlex
import time

from shipconfiguration import ShipConfiguration, Ship, ShipCreationError

SHIP_LENGTHS = [4, 3, 3, 3, 2, 2]
SHOTS_PATTERN = re.compile(r"(\d+) rounds")

LOG_FILE = "test.log"
JSON_FILE = "test.json"

logger = logging.getLogger(__name__)


class BattleshipTest(object):
    def __init__(self, client, server,
                 samples=100, strict=True, verbose=False):
        if type(client) is not str:
            raise TypeError("Client has to be of type str.")
        if type(server) is not str:
            raise TypeError("Server has to be of type str.")
        if type(samples) is not int:
            raise TypeError("Samples has to be of type int.")
        if type(strict) is not bool:
            raise TypeError("Strict has to be of type bool.")
        if type(verbose) is not bool:
            raise TypeError("Verbose has to be of type bool.")

        self.client = client
        self.server = server
        self.samples = samples
        self.strict = strict
        self.verbose = verbose

        if verbose:
            level = logging.DEBUG
        else:
            level = logging.INFO

        logger.setLevel(level)

        fmt = "%(asctime)s [%(levelname)s] %(message)s"
        datefmt = "%Y-%m-%d %H:%M:%S"
        formatter = logging.Formatter(fmt=fmt, datefmt=datefmt)

        sh = logging.StreamHandler(sys.stdout)
        sh.setFormatter(formatter)
        logger.addHandler(sh)

        fh = logging.FileHandler(LOG_FILE, "w")
        fh.setFormatter(formatter)
        logger.addHandler(fh)

    def start_test(self):
        self.tests = []
        n = 0

        try:
            while n < self.samples:
                msg = "Test {} of {}."
                logger.debug(msg.format(n + 1, self.samples))

                try:
                    logger.debug("Starting test.")
                    stdout = self._run_test()
                    logger.debug("Processing test.")
                    test = self._process_test(stdout)

                    self.tests.append(test)
                    self.log_statistics()

                    n += 1
                except TestException as e:
                    msg = "Caught exception while testing: {}"
                    logger.warning(msg.format(e.msg))

                    if not self.strict:
                        n += 1
        except KeyboardInterrupt:
            pass

        with open(JSON_FILE, "w") as json_file:
            json.dump(self.tests, json_file, indent="\t")

    def log_statistics(self):
        test_count = len(self.tests)
        test_sum = sum(self.tests)
        test_mean = test_sum / float(max(test_count, 1))

        msg = "test count: {}, test sum: {}, test mean: {}"
        logger.info(msg.format(test_count, test_sum, test_mean))

    def _process_test(self, stdout):
        shots = re.search(SHOTS_PATTERN, stdout)
        if self.strict and shots is None:
            raise TestException("Client lost.")

        shots_count = shots.group(1)
        logger.info("Client needed {} shots.".format(shots_count))

        return int(shots_count)

    def _run_test(self):
        sc = generate_ship_configuration(SHIP_LENGTHS)
        scstr = str(sc)
        logger.info("Generated ship layout: " + scstr)

        cmd = self.server + " " + scstr
        logger.debug("Starting server using: " + cmd)
        args = shlex.split(cmd)
        p0 = subprocess.Popen(args,
                              stdout=subprocess.PIPE,
                              stderr=subprocess.DEVNULL)

        # give server time to bind to interface
        time.sleep(0.05)

        cmd = self.client
        logger.debug("Starting client using: " + cmd)
        args = shlex.split(cmd)
        p1 = subprocess.Popen(args,
                              stdout=subprocess.DEVNULL,
                              stderr=subprocess.DEVNULL)

        logger.debug("Waiting for client to terminate.")
        try:
            p0.wait(timeout=5)
            p1.wait(timeout=5)
            logger.debug("Client terminated.")
        except subprocess.TimeoutExpired as e:
            raise TestException("Client timed out.")

        stdout, _ = p0.communicate()
        stdout = stdout.decode(encoding="UTF-8")

        return stdout


class TestException(Exception):
    def __init__(self, msg):
        self.msg = msg


def generate_ship_configuration(ship_lengths):
    sc = ShipConfiguration(size=10)

    for slen in ship_lengths:
        success = False

        while not success:
            sx = random.randint(0, 9)
            ex = sx
            sy = random.randint(0, 9)
            ey = sy

            rand = random.randint(0, 3)
            if rand == 0:
                ex += (slen - 1)
            elif rand == 1:
                ex -= (slen - 1)
            elif rand == 2:
                ey += (slen - 1)
            else:
                ey -= (slen - 1)

            try:
                s = Ship(sx, sy, ex, ey)
            except ShipCreationError:
                continue

            success = sc.add_ship(s)

    return sc
