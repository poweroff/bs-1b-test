#!/usr/bin/env python3

import sys
import json

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter

if len(sys.argv) != 2:
    print("No data file given.")
    exit(1)

with open(sys.argv[1]) as json_file:
    data = json.load(json_file)

def to_percent(y, position):
    s = str(100 * y)
    return s + "%"

plt.hist(data, bins=range(20,80), normed=True)
formatter = FuncFormatter(to_percent)
plt.gca().yaxis.set_major_formatter(formatter)
plt.show()
